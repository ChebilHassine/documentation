# Performance des script

- donnant leur nature bloquante, il est préférable de mettre les balises `<scrip>` à la fin du `<body>` au lieu du `<head>`.
- Seuls les scripts qui ont besoin d'être exécuté afin de créer/modifier la structure de la page sont à mettre dans le `<head>`
- Un script, qui ne modifie pas le `DOM` la structure primaire dont on a besoin pour créer le premier affichage, doit contenir l'attribut [<tooltip term="defer">`defer`</tooltip>](https://developer.mozilla.org/fr/docs/Web/HTML/Element/script#defer)

## Arbre de rendu (Rendering Tree) : 
- contient 2 étapes Reflow (Mise en page) et Painting (Peinture),
- l'étape de <tooltip term="reflow">Mise en page (Reflow)</tooltip> est une étape couteuse que les navigateurs optimize à l'aide d'algorithme native de mise en file d'attente des modifications (Queuing changes),
Par contre des manipulations du dom avec des utilitaires tel que :
- offsetTop, offsetLeft, offsetWidth, offsetHeight
- scrollTop, scrollLeft, scrollWidth, scrollHeight
- clientTop, clientLeft, clientWidth, clientHeight
- getComputedStyle()

Peuvent supprimer la liste d'attente des changements mis en place par le navigateur et implique l'application des changements tout de suite. Car les méthodes et propriétés ci-dessus de l'API DOM et CSS OM (getComputedStyle()) ont besoin des valeurs à jour immédiatement.

En se basant sur l'exemple du html suivant : 
```html
<ul id="mylist">
   <li><a href="http://phpied.com">Stoyan</a></li>
   <li><a href="http://julienlecomte.com">Julien</a></li>
</ul>
```
On veut ajouter des entrées à la liste en minimisant le Reflow, pour y parvenir, il faut suivre l'une des trois méthodes suivantes :
- cacher l'élément, appliquer les modifications, afficher l'élément :

<code-block lang="javascript" collapsible="true">
    const data = [
      {
        "name": "Nicholas",
        "url": "http://nczonline.net"
      },
      {
        "name": "Ross",
        "url": "http://techfoolery.com"
      }
    ];
    function appendDataToElement(appendToElement, data) {
        let a, li;
        for(let i=0, len=data.length; i <![CDATA[<]]> len; i++) {
            const entry = data[i];
            a = document.createElement('a');
            a.href = entry.url;
            a.appendChild(document.createTextNode(entry.name));
            li = document.createElement('li');
            li.appendChild(a);
            appendToElement.appendChild(li);
        }
    }
    const ul = document.getElementById('mylist');
    ul.style.display = 'none'; //cacher l'élément
    appendDataToElement(ul, data); //appliquer les modifications
    ul.style.display = 'block'; //afficher l'élément
</code-block>

- Utiliser un fragment du document pour créer un sous-arbre du DOM, appliquer les modifications sur le fragment puis injecter le fragment dans le DOM.

<code-block lang="javascript" collapsible="false">
const fragment = document.createDocumentFragment();
appendDataToElement(fragment, data);
document.getElementById('mylist').appendChild(fragment);
</code-block>

- Cloner l'élément à modifier dans un nœud apart, appliquer les modifications sur le clone, puis remplacer l'original avec le clone.

<code-block lang="javascript" collapsible="false">
const old = document.getElementById('mylist');
const clone = old.cloneNode(true);
appendDataToElement(clone, data);
old.parentNode.replaceChild(clone, old);
</code-block>


## Trés intéressant besoin de traduction en français
A technique to avoid a reflow of a big part of the page is to use the following steps:

Use absolute positioning for the element you want to animate on the page, taking it out of the layout flow of the page.

Animate the element. When it expands, it will temporarily cover part of the page. This is a repaint, but only of a small part of the page instead of a reflow and repaint of a big page chunk.

When the animation is done, restore the positioning, thereby pushing down the rest of the document only once.


## conditional

- if < switch < array

```Javascript
// slowest
function className(color) {
    if(color === "red") {
        return className = 'red';
    } else if(color === "green") {
        return className = 'green';
    } else if(color === "white") {
        return className = 'white';
    } else if ...
    else { 
        return className = '';
    }
}


//faster then the if chain
function className(color) {
    switch(color) {
        case "red": 
            return className = 'red';
            break;
        case "green":
            return className = 'green';
            break;
        case "white":
            return className = 'white';
            break;
        ...
        default:
            return className = 'white';
            break;
    }
}

// The fastest array access 
function className(color) { 
    const colors = ['red', 'green', 'white'];
    return colors.find(entry => entry === color) || '';
}
```

## Array Processing with Timers

```Javascript
function processArray(items, process, callback){
    var todo = items.concat();   //create a clone of the original

    setTimeout(function(){
        process(todo.shift());

        if (todo.length > 0){
            setTimeout(arguments.callee, 25);
        } else {
            callback(items);
        }

    }, 25);    
}

var items = [123, 789, 323, 778, 232, 654, 219, 543, 321, 160];

function outputValue(value){
    console.log(value);
}

processArray(items, outputValue, function(){
    console.log("Done!");
});
```