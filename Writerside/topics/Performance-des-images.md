# Performance des images

Les images sont les ressources les plus lourdes et les plus utilisés sur nos sites. D'où l'importance primordiale de leur optimisation.

Voici un article de ***web.dev*** qui explique bien ce sujet : [*Performances des images*](https://web.dev/learn/performance/image-performance?continue=https%3A%2F%2Fweb.dev%2Flearn%2Fperformance&hl=fr#article-https://web.dev/learn/performance/image-performance&hl=fr)

## 1. Choix de type de l'image

En tant que dev dès la récupération de la maquette, il faut bien choisir le type de la ressource image à utiliser :

<table>
<tr><td>Use case</td><td>Type d'image</td></tr>
<tr><td>icon</td><td>Changer en fontIcon au pire du <control>svg</control></td></tr>
<tr><td>Image avec transparence</td><td>webp <font color="red">(si l'extension n'est pas disponible <control>png</control>)</font></td></tr>
<tr><td>Image sans transparence</td><td>webp <font color="red">(si l'extension n'est pas disponible <control>jpg</control>)</font></td></tr>
</table>

## 2. Choix de la taille de l'image

Afin de supporter la majorité des supports digitaux, il faut choisir la bonne taille selon ce support.

<table>
<tr><td>Support</td><td>Taille</td></tr>
<tr><td>Smartphone</td><td>767</td></tr>
<tr><td>Tablette, Ordinateur portable</td><td>1024</td></tr>
<tr><td>Ordinateur de Bureau</td><td>1280</td></tr>
</table>

Donc, on doit toujours avoir 3 tailles de la même image adéquate au type de support.

Ses trois tailles peuvent être fournies directement sur la maquette, sinon on prend l'image la plus grande et à l'aide d'outils d'optimisation, on génère les trois tailles selon la config ci-dessus.

## 3. Téchnique d'amélioration de pérformance d'image
Pour améliorer les pérformance des ressources image en plus du bon choix de type et de la taille de l'image, le navigateur nous propose une balise HTML trés puissante qui lui permet de choisir l'image la plus adéquate. C'est la balise [***picture***](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/picture).

La balise picture qui profite de toute amélioration possible :

```HTML
<picture>
    <!-- 
        Explication :
        * Dans la balise img on a mis l'image la plus petite et la plus
        disponible niveau extension en cas de mauvaise connexion ou si le
        navigateur ne supporte pas les extention nouvelle génération

        * On utilise la balise source avec l'attribut media pour donner l'information au
        navigateur quelle taille de fichier adéquat à l'écran d'affichage
        et en regardant quel type d'image le mieux supporter et le plus performant pour le navigateur

        * Comment le navigateur réagis à cette balise :
        1. quelle meilleur extension je supporte ? -> 
        2. parcours les balise sources -> 
        3. trouve la bonne extension -> 
        4. quelle taille d'image je dois télècharger ? ->
        5 .trouve la bonne image grâce à l'attribut media.

        Ce processus est native et hyper rapide donc oui ça améliore les performances
     -->
    <source srcset="img-mobile.webp" type="image/webp" media="(max-width: 767px)">
    <source srcset="img-tablet.webp" type="image/webp" media="(min-width: 767px) and (max-width: 1023px)">
    <source srcset="img-desktop.webp" type="image/webp" media="(min-width: 1024px)">
    <source srcset="img-tablette.jpg" media="(min-width: 767px) and (max-width: 1023px)">
    <source srcset="img-desktop.jpg" media="(min-width: 1024px)">
    <img loading="lazy" width="500" height="500" src="img-mobile.jpg"/>
</picture>
```
> Pour aider le navigateur à choisir la bonne image. Il faut toujours commencer par la source d'image le plus optimisé et de plus petite taille (mobile first), donc dans l'exemple ci-dessus, on a choisi .avif (mobile → tablette → desktop) → .webp (mobile → tablette → desktop) → .jpg (tablette → desktop) et par défaut notre img mobile.jpg
>
{style="note"}
> Notre choix qui implique la taille de l'image selon le support utilise la densité par défaut du navigateur le x1. Cela signifie que chaque pixel dans une image correspond à un pixel physique sur l'écran de l'appareil.
>
{style="warning"}