> La version actuelle de l'outil supporte que les versions `node ≤ 14`.
> 
{style="warning"}
# Outil d&apos;optimization d&apos;image (optImg)

[**OptiImg**](https://gitlab-dogen.group.echonet/market-place/ap00914/sandbox/optiimg) est un projet qui permet d'optimiser des images en lançant une ligne de commande.


Dans ce tutorial on va apprendre:

- Comment installer l'outil
- Comment configurer l'outil
- Comment utiliser l'outil

## Comment installer l'outil
> Il faut impérativement avoir la bonne configuration du npm dans le fichier `.npmrc` un fichier `npmrc-exemple` 
> est fourni avec le projet, il faut le renommer en `.npmrc`. Une documentation sur la config npm se trouve [ici](Npm-Config-with-proxy.md).
> 
{style="warning"}
```Bash
# Cloner le porjet git dans le dossier/workspace de votre choix
git clone https://gitlab-dogen.group.echonet/market-place/ap00914/sandbox/optiimg
```
```Bash
# Se positionner sur le projet
cd optiimg
```
```Bash
# Installer les dépendances npm, en cas d'erreur vérifier que votre fichier .npmrc est bien configuré.
npm i
```
## Comment configurer l'outil

La configuration de l'outil est simple. On a :

- Un fichier de configuration `config.json`.
- Un dossier images qui va contenir les images à optimiser `src/images`.

Le fichier `config.json` est un <tooltip term="json">JSON</tooltip>, qui contient trois nœuds principaux :
- `nextGenExt`: permet de mettre en place les différentes extensions qu'on souhaite que l'outil d'optimisation squoosh transforme :
```JSON
  "nextGenExt": {
    "active": true,
    "extensions": {
      "webp": {}
    }
  }
```
> Dans ce cas l'outil va optimiser/compresser et transformer les images vers l'extension `webp`. Par contre, si on paramètre 
> `nextGenExt.active` à false, l'outil ignore l'étape de transformation et va juste optimiser/compresser l'image.
> On peut ajouter les extensions au nœud `exetnsions`: `"avif": {}, "mozjpeg": {}, "jxl": {}, "wp2": {}, "oxipng": {}`
>
{style="note"}
- `resize`: qui permet de configurer les tailles d'images qui seront générer selon l'écran (mobile, tablet, desktop). Exemple :
```json 
  "resize": {
    "active": true,
    "desktop": {
      "width": 1280
    },
    "tablet": {
      "width": 1024
    },
    "mobile": {
      "width": 767
    }
  }
```
> Dans ce cas l'outil va comparer la largeur de l'image source par rapport à la largeur donner dans la config.
> Si la largeur dépasse la limite l'outil va faire une transformation de taille pour respecter la règle.
> Sinon aucune transformation de taille ne sera effectué.
> 
{style="note"}

> Dans le cas où on paramètre `resize.active` à false, l'outil va juste optimiser l'image sans renommer l'image avec des suffix `-mobile`, `-desktop` ou `-tablet`, ni créer de dossier qui groupe toutes les duplications.
>
{style="note"}

- `optimize`: qui permet de configurer sur quelle taille d'écran on veut optimiser nos images. ça permet aussi 
de choisir si on veut transformer les images vers des nextGen (webp, avif ...) ou bien juste optimiser les extensions 
d'origine (jpg, png, svg) ou bien les deux. Exemple :
```json
  "optimize": {
    "mobile": {
      "active": true,
      "nextGen": true,
      "original": false
    },
    "tablet": {
      "active": false,
      "nextGen": true,
      "original": true
    },
    "desktop": {
      "active": true,
      "nextGen": true,
      "original": false
    }
  }
```
> Dans ce cas l'outil va optimiser les images pour les écrans mobile est desktop car, `tablet.active` est `false`.
> L'outil va aussi optimiser et transformer nos images vers des images nextGen parce que `mobile.nextGen` et `desktop.nextGen` sont `true`, par contre
> pas d'optimisation de l'extension originale de la source puisque `mobile.nextGen` et `desktop.nextGen` sont `false`.
>
{style="note"}

> Attention : il est conseillé d'utiliser l'outil pour optimiser des images avec les extensions suivantes `jpg`, `png` et `svg`.
> Il est fermement déconseillé de l'utiliser pour optimiser des images nextGen `webP` ou `avif`, car ils sont déjà optimisés et que 
> les optimisés un deuxième fois va diminuer la qualité de votre image.
> 
{style="warning"}

## Comment utiliser l'outil
Une fois la configuration est faite, il suffit de lancer la commande suivante sur le terminal :
```Bash
npm run optimize
```

Les images optimisées seront alors générer dans le dossier `build`.

> Astuce : s'il y a des icons ou picto qui ne sont pas en `svg`, par exemple en `jpg` ou `png`.
> Créer un dossier `icons` dans le dossier `images` directement ou bien dans votre arborescence `images/votreNomDeDossier/icons`, 
> pour l'outil ne les prend pas comme cible d'optimisation d'image par taille d'écran.