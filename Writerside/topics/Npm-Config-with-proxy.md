# Npm Config with proxy
Afin de pouvoir utiliser le Node Package Manager d'une manière fluide sans blocage proxy (ça reste limité), il faut
ajouter la configuration proxy dans le fichier de conf npm `.npmrc`, comme suit :

```Shell
# enléve le self-signed certificate en local
strict-ssl=false
# le registery npm global sur artifactory
registry=https://repo.artifactory-dogen.group.echonet/artifactory/api/npm/npm/ 
# garde la connexion en mémoire après le premier npm login
always-auth=true
#config du proxy
proxy=http://uid:mdpAnnuaire@ncproxy:8080/
#config du proxy pour des requête en HTTP
http-proxy=http://uid:mdpAnnuaire@ncproxy:8080/
#config du proxy pour des requête en HTTPS
https-proxy=http://uid:mdpAnnuaire@ncproxy:8080/
```

En cas de pérsistance du problème self signed certificat, il faut exécuter cette commande sur le terminal (Pour les 
utilisateurs windows de préférence sur un terminal bash) :
```Shell
export NODE_TLS_REJECT_UNAUTHORIZED=0;
```