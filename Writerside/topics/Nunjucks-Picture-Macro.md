# Nunjucks Picture Macro

La <tooltip term="macro">macro</tooltip> Picture permet de générer la structure de la balise img ou picture en prenant compte des recommendations web perf.

Dans ce tutorial on va apprendre:
* Comment utiliser la macro Picture
* Les différents paramètres pris en charge par la macro
* Les différents cas d'usage de la macro

## Comment utiliser la macro Picture

Pour utiliser la macro rien de plus simple, sur la template nunjucks de votre choix, il suffit d'inclure le code suivant :
```html
{% macro picture(name, originalExt, alt = '', height = '' , width = '', path = '', class,
supportOldBrowz = false, nextGen = true, forMobile = true, forTablet = true,
forDesktop = true, justAnImg = false ) %} {% set imgUri = name + "/" + name %}
{% if name and originalExt and height and width %}
{% if not justAnImg %}
<picture>
    {% if nextGen %}
    {% if forMobile %}
    <source
            srcset="{{path | escape}}/{{ imgUri | escape}}-mobile.webp"
            type="image/webp"
            media="(min-width: 320px) and (max-width: 768px)"
    />
    {% endif %}
    {% if forTablet %}
    <source
            srcset="{{path | escape}}/{{ imgUri | escape}}-tablet.webp"
            type="image/webp"
            media="(min-width: 769px) and (max-width: 1023px)"
    />
    {% endif %}
    {% if forDesktop %}
    <source
            srcset="{{path | escape}}/{{ imgUri | escape}}-desktop.webp"
            type="image/webp"
            {% if forTablet == true %}
            media="(min-width: 1024px)"
            {% endif %}
            {% if forTablet == false and forMobile == false %}
            media="(min-width: 1024px)"
            {% endif %}
    />
    {% endif %}
    {% endif %}
    {% if supportOldBrowz %}
    {% if forTablet %}
    <source
            srcset="{{path | escape}}/{{ imgUri | escape}}-tablet.{{ originalExt | escape}}"
            media="(min-width: 767px) and (max-width: 1023px)"
    />
    {% endif %}
    {% if forDesktop %}
    <source
            srcset="{{path | escape}}/{{ imgUri | escape}}-desktop.{{ originalExt | escape}}"
            {% if forTablet == true %}
            media="(min-width: 1024px)"
            {% endif %}
            {% if forTablet == false and forMobile == false %}
            media="(min-width: 1024px)"
            {% endif %}
    />
    {% endif %}
    {% endif %}
    <img
            loading="lazy"
            height="{{ height }}"
            width="{{ width }}"
            class="{{ class }}"
            src="{{path | escape}}/{{imgUri | escape}}-mobile.{{ originalExt | escape}}"
            alt="{{alt | escape}}"
    />
</picture>
{% else %}
<img
        loading="lazy"
        height="{{ height }}"
        width="{{ width }}"
        class="{{ class }}"
        src="{{path | escape}}/{{name | escape}}.{{ originalExt | escape}}"
        alt="{{alt | escape}}"
/>
{% endif %}
{% else %}
<!-- Error while generating picture element
   * name of the ressource is mandatory to generate the picture element
   * originalExt of the ressource is mandatory to generate the picture element 
   * hieght and width of the image are mendatory to generate the picture element
   -->
{% endif %}
{% endmacro %}
```
{collapsible="true"}

Puis appeler la macro avec le mot clé `picture` dans le html, comme suit :

```html
<section>
   <header>
      {{ picture(name="imgName", originalExt="png", height="500", width="500", path="path/to/img") }}
   </header>
</section>
```
{collapsible="true"}

## Les différents paramètres pris en charge par la macro

La macro prend en charge plusieurs paramètres, 4 sont obligatoires les autres sont optionnels.
<table>
<tr>
<td colspan="3"><b>Les paramètres <font color="red">Obligatoires</font></b></td>
</tr>
<tr>
   <td><code>name</code></td>
   <td><code>string</code></td>
   <td>Le nom de l'image sans extension</td>
</tr>
<tr>
   <td><code>originalExt</code></td>
    <td><code>string</code></td>
   <td>L'extension de l'image</td>
</tr>
<tr>
   <td><code>height</code></td>
    <td><code>number</code></td>
   <td>La hauteur de l'image qui va être reservé par le navigateur</td>
</tr>
<tr>
   <td><code>width</code></td>
   <td><code>number</code></td>
   <td>La largeur de l'image qui va être reservé par le navigateur</td>
</tr>
</table>

<table>
<tr>
<td colspan="3"><b>Les paramètres Optionnels</b></td>
</tr>
<tr>
   <td><code>alt</code></td>
    <td><code>string</code></td>
   <td>Le text qui va enrichir l'attribut alt, par défaut, il est vide.</td>
</tr>
<tr>
   <td><code>supportOldBrowz</code></td>
<td><code>boolean</code></td>
   <td>Ajoute une source pour les extensions non next-gen, définis à <code>false</code> par défaut.</td>
</tr>
<tr>
   <td><code>nextGen</code></td>
<td><code>boolean</code></td>
   <td>Ajoute les sources pour les images next-gen (webp, avif), définis à <code>true</code> par défaut.</td>
</tr>
<tr>
   <td><code>forMobile</code></td>
<td><code>boolean</code></td>
   <td>Ajoute la source pour les écrans mobile en ajoutant la règle "media" adéquate, définis à <code>true</code> par défaut.</td>
</tr>
<tr>
   <td><code>forTablet</code></td>
<td><code>boolean</code></td>
   <td>Ajoute la source pour les écrans tablette en ajoutant la règle "media" adéquate, définis à <code>true</code> par défaut.</td>
</tr>
<tr>
   <td><code>forDesktop</code></td>
<td><code>boolean</code></td>
   <td>Ajoute la source pour les écrans desktop en ajoutant la règle "media" adéquate, définis à <code>true</code> par défaut.</td>
</tr>
<tr>
   <td><code>justAnImg</code></td>
<td><code>boolean</code></td>
   <td>Quand elle est définis à <code>true</code> la macro va générer une balise image au lieu d'une balise picture, définis à <code>false</code>par défaut.</td>
</tr>
<tr>
   <td><code>path</code></td>
<td><code>string</code></td>
   <td>Le chemin spécific de l'image, par défaut, il est vide.</td>
</tr>
<tr>
   <td><code>class</code></td>
    <td><code>string</code></td>
   <td>Les classes à ajouter sur la balise img, une chaine de caractère séparé par un espace <code xml:lang="javascript">'classe1 classe2'</code></td>
</tr>
</table>

## Les différents cas d'usage de la macro

<tabs>
    <tab title="Par d\éfaut">
        <code-block lang="twig">
            {{ picture(name='hello', originalExt='jpg', height=500, width=500) }}
        </code-block>
        → Résultat :
        <code-block collapsible="true" lang="twig">
            <![CDATA[
            <picture>
              <source
                srcset="/hello/hello-mobile.webp"
                type="image/webp"
                media="(min-width: 320px) and (max-width: 768px)"
              />
              <source
                srcset="/hello/hello-tablet.webp"
                type="image/webp"
                media="(min-width: 769px) and (max-width: 1023px)"
              />
              <source
                srcset="/hello/hello-desktop.webp"
                type="image/webp"
                media="(min-width: 1024px)"
              />
              <img
                loading="lazy"
                height="500"
                width="500"
                class=""
                src="/hello/hello-mobile.jpg"
                alt=""
              />
            </picture>
            ]]>
        </code-block>
    </tab>
    <tab title="Avec un chemin sp\écifique">
        <code-block lang="twig">
            {{ picture(name='hello', originalExt='jpg', height=500, width=500, path="path/to/img") }}
        </code-block>
        → Résultat :
        <code-block collapsible="true" lang="twig">
            <![CDATA[
                <picture>
                  <source
                    srcset="path/to/img/hello/hello-mobile.webp"
                    type="image/webp"
                    media="(min-width: 320px) and (max-width: 768px)"
                  />
                  <source
                    srcset="path/to/img/hello/hello-tablet.webp"
                    type="image/webp"
                    media="(min-width: 769px) and (max-width: 1023px)"
                  />
                  <source
                    srcset="path/to/img/hello/hello-desktop.webp"
                    type="image/webp"
                    media="(min-width: 1024px)"
                  />
                  <img
                    loading="lazy"
                    height="500"
                    width="500"
                    class=""
                    src="path/to/img/hello/hello-mobile.jpg"
                    alt=""
                  />
                </picture>
            ]]>
        </code-block>
    </tab>
    <tab title="Enrichir les attributs alt et class">
        <code-block lang="twig">
            {{ picture(name='hello', originalExt='jpg', height=500, width=500, path="path/to/img", class="classe1 classe2", alt="un text descriptif") }}
        </code-block>
        → Résultat :
        <code-block collapsible="true" lang="twig">
            <![CDATA[
                <picture>
                  <source
                    srcset="path/to/img/hello/hello-mobile.webp"
                    type="image/webp"
                    media="(min-width: 320px) and (max-width: 768px)"
                  />
                  <source
                    srcset="path/to/img/hello/hello-tablet.webp"
                    type="image/webp"
                    media="(min-width: 769px) and (max-width: 1023px)"
                  />
                  <source
                    srcset="path/to/img/hello/hello-desktop.webp"
                    type="image/webp"
                    media="(min-width: 1024px)"
                  />
                  <img
                    loading="lazy"
                    height="500"
                    width="500"
                    class="classe1 classe2"
                    src="path/to/img/hello/hello-mobile.jpg"
                    alt="un text descriptif"
                  />
                </picture>
            ]]>
        </code-block>
    </tab>
    <tab title="Choisir la taille de l'\écran \à supporter">
        <code-block lang="twig">
            {{ picture(name='hello', originalExt='jpg', height=500, width=500, path="path/to/img", forTablet=false, forDesktop=true, forMobile=true) }} 
        </code-block>
→ Résultat :
        <code-block collapsible="true" lang="twig">
            <![CDATA[
                <picture>
                  <source
                    srcset="path/to/img/hello/hello-mobile.webp"
                    type="image/webp"
                    media="(min-width: 320px) and (max-width: 768px)"
                  />
                  <source srcset="path/to/img/hello/hello-desktop.webp" type="image/webp" />
                  <img
                    loading="lazy"
                    height="500"
                    width="500"
                    class=""
                    src="path/to/img/hello/hello-mobile.jpg"
                    alt=""
                  />
                </picture>
            ]]>
        </code-block>
    </tab>
    <tab title="G\én\érer la balise img seulement">
        <code-block lang="twig">
            {{ picture(name='hello', originalExt='jpg', height=500, width=500, path="path/to/img", justAnImg=true) }} 
        </code-block>
→ Résultat :
        <code-block lang="twig">
            <![CDATA[
            <img
              loading="lazy"
              height="500"
              width="500"
              class=""
              src="path/to/img/hello.jpg"
              alt=""
            />
            ]]> 
        </code-block>
</tab>
</tabs>